using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{
    public GameOver GameOver;
    public Enemies enemy;
    public GameObject Ult;
    public GenerateEnemy genEn;

    private Vector2 Touchpos;
    private Vector2 Touchcur;
    private Vector2 Touchend;
    private bool TS = false;
    private Vector2 targetPos;
   

        private float width;
        private float height;

        public float swipeRange;
        public float tapRange;

        public int hitComp;
        public GameObject player;
    public GameObject enemies;
    public GameObject boss;
    

    public float speed;

        public Direction myDirection;

    public int healthP = 10;
    public int pointsC;
    public int mpBlast;

    public void GameOverS()
    {
        GameOver.Setup(pointsC);
    }
        // Start is called before the first frame update
        void Start()
        {
            healthP = 10;
        }

        void Awake()
        {
            width = (float)Screen.width / 2.0f;
            height = (float)Screen.height / 2.0f;
        }

        // Update is called once per frame
        void Update()
        {
        transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);

            if (Input.touchCount == 1)
            {
                Touch myTouch = Input.GetTouch(0);

                if (myTouch.phase == TouchPhase.Began)
                {
                    Touchcur = myTouch.rawPosition;
                    
                }
                if (myTouch.phase == TouchPhase.Moved)
                {
                    //Touchpos = Touchcur - myTouch.position;
                    Touchpos = myTouch.deltaPosition;
                    Touchend = Touchcur + Touchpos;
                   
                    if (Touchcur.y < Touchend.y && Touchcur.x == Touchend.x)
                    {
                        myDirection = Direction.up;
                        //textValue = "Up";
                        hitComp = 1;
                        targetPos = new Vector2(transform.position.x, transform.position.y + Touchend.y);
                        if (enemies = GameObject.FindGameObjectWithTag("mookUp"))
                        {
                            pointsC += 1;
                            mpBlast += 1;
                        enemies = GameObject.FindGameObjectWithTag("mookUp");
                        Destroy(enemies);
                        if (boss = GameObject.FindGameObjectWithTag("boss"))
                        {
                            genEn.bossHP--;
                            if(genEn.bossHP<=0)
                            {
                                Destroy(boss);
                                genEn.bossHP = 10;
                            }
                        }
                    }
                        //else if (hitComp != enemy.hitArrow)
                        //{
                        //    healthP -= 1;
                        //}
                    }
                    if (Touchcur.y > Touchend.y && Touchcur.x == Touchend.x)
                    {
                        myDirection = Direction.down;
                        //textValue = "Down";
                        hitComp = 2;
                        targetPos = new Vector2(transform.position.x, transform.position.y - Touchend.y);
                    if (enemies = GameObject.FindGameObjectWithTag("mookDown"))
                    {
                        pointsC += 1;
                        mpBlast += 1;
                        enemies = GameObject.FindGameObjectWithTag("mookDown");
                        Destroy(enemies);
                        if (boss = GameObject.FindGameObjectWithTag("boss"))
                        {
                            genEn.bossHP--;
                            if (genEn.bossHP <= 0)
                            {
                                Destroy(boss);
                                genEn.bossHP = 10;
                            }
                        }
                    }
                    //else if (hitComp != enemy.hitArrow)
                    //{
                    //    healthP -= 1;
                    //}
                }
                    if (Touchcur.x < Touchend.x && Touchcur.y == Touchend.y)
                    {
                        myDirection = Direction.right;
                        //textValue = "Right";
                        hitComp = 3;
                        targetPos = new Vector2(transform.position.x + Touchend.x, transform.position.y);
                    if (enemies = GameObject.FindGameObjectWithTag("mookRight"))
                    {
                        pointsC += 1;
                        mpBlast += 1;
                        enemies = GameObject.FindGameObjectWithTag("mookRight");
                        Destroy(enemies);
                        if (boss = GameObject.FindGameObjectWithTag("boss"))
                        {
                            genEn.bossHP--;
                            if (genEn.bossHP <= 0)
                            {
                                Destroy(boss);
                                genEn.bossHP = 10;
                            }
                        }
                    }
                    //else if (hitComp != enemy.hitArrow)
                    //{
                    //    healthP -= 1;
                    //}
                }
                    if (Touchcur.x > Touchend.x && Touchcur.y == Touchend.y)
                    {
                        myDirection = Direction.left;
                        //textValue = "Left";
                        hitComp = 4;
                        targetPos = new Vector2(transform.position.x - Touchend.x, transform.position.y);
                    if (enemies = GameObject.FindGameObjectWithTag("mookLeft"))
                    {
                        pointsC += 1;
                        mpBlast += 1;
                        enemies = GameObject.FindGameObjectWithTag("mookLeft");
                        Destroy(enemies);
                        if (boss = GameObject.FindGameObjectWithTag("boss"))
                        {
                            genEn.bossHP--;
                            if (genEn.bossHP <= 0)
                            {
                                Destroy(boss);
                                genEn.bossHP = 10;
                            }
                        }
                    }
                    //else if (hitComp != enemy.hitArrow)
                    //{
                    //    healthP -= 1;
                    //}
                }   
                    //textElement.text = textValue;
                }//end touchphase.moved
                if (myTouch.phase == TouchPhase.Ended)
                {
                    //textElement.text = "No input";
                }
            }//end input touch count
            if (healthP <= 0)
            {
                GameOverS();
            }
            if (mpBlast >= 10)
            {
                Ult.SetActive(true);
            }
           
        }
    public void SmashingStrike() //not sure if this logic works for ults please check
    {
        for (int i = 0; i < 4; i++)
        {
            enemy.hp -= 10;
        }
    }
}