using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateEnemy : MonoBehaviour
{
    [SerializeField]
    private GameObject[] Enemies;
    [SerializeField]
    private GameObject theBoss;
    public float bossHP = 10;

    private BoxCollider2D col;

    public Player player;
    public int i = 0;
    float x1, x2;

    private void Awake()
    {
        col = GetComponent<BoxCollider2D>();
        x1 = transform.position.x - col.bounds.size.x / 2f;
        x2 = transform.position.x + col.bounds.size.x / 2f;
    }

    private void Start()
    {
        StartCoroutine(SpawnMook(1f));
    }

    private void Update()
    {
        if (i >= 10)
        {

            StartCoroutine(SpawnBoss(1f));
            i = 0;
        }

    }
    IEnumerator SpawnMook(float time)
    {
        yield return new WaitForSecondsRealtime(time);

        Vector3 temp = transform.position;
        temp.x = Random.Range(x1, x2);

        Instantiate(Enemies[Random.Range(0, Enemies.Length)], temp, Quaternion.identity);
        i++;
        
            StartCoroutine(SpawnMook(Random.Range(1f, 2f)));
        
        if (player.healthP <= 0)
        {
            StopCoroutine(SpawnMook(1f));
        }
        
    }

    IEnumerator SpawnBoss(float time)
    {
        yield return new WaitForSecondsRealtime(time);
        
        Vector3 temp = transform.position;
        temp.x = Random.Range(x1, x2);

        Instantiate(theBoss, temp, Quaternion.identity);
        i++;
        StopCoroutine(SpawnBoss(Random.Range(1f, 2f)));
    }


   
    //public class EnemyClass
    //{
    //    private EnemyClass nextEnemy;
    //    private EnemyClass prevEnemy;
    //    private BoxCollider2D col;
    //    private int hp;
    //    private GameObject[] arrows;
    //    private int[] arrsel;
    //    private GameObject enemyspr;
    //    private string name;
    //    private Vector2 position;
    //    private static System.Random r = new System.Random();

    //    public EnemyClass()
    //    {
    //        hp = 0;
    //        name = "";
    //    }

    //    public int getHp()
    //    {
    //        return hp;
    //    }

    //    public void setHp(int hp)
    //    {
    //        this.hp = hp;
    //    }

    //    public string getName()
    //    {
    //        return name;
    //    }

    //    public void setName(string name)
    //    {
    //        this.name = name;
    //    }

    //    public void setPosition(Vector2 position)
    //    {
    //        this.position = position;
    //    }

    //    public Vector2 getPosition()
    //    {
    //        return position;
    //    }

    //    public void setNextEnemy(EnemyClass next)
    //    {
    //        this.nextEnemy = next;
    //    }

    //    public void setPrevEnemy(EnemyClass prev)
    //    {
    //        this.prevEnemy = prev;
    //    }

    //    public EnemyClass getNextEnemy()
    //    {
    //        return this.nextEnemy;
    //    }

    //    public EnemyClass getPreviousEnemy()
    //    {
    //        return this.prevEnemy;
    //    }

    //    public void initializeSequenceMook()
    //    {

    //        this.arrsel = new int[2];
    //        this.arrsel[0] = r.Next(0, 5);
    //        this.arrsel[1] = 0;
    //    }

    //    public void initializeSequenceBoss()
    //    {
    //        this.arrsel = new int[4];
    //        for (int i = 0; i < 4; i++)
    //        {
    //            this.arrsel[i] = r.Next(0, 5);
    //        }
    //    }

    //}

    //public GameObject[] arrows;
    //public GameObject mook;
    //public GameObject boss;
    //private EnemyClass enem;
    //public Destroyexcess destroy;

    //private float x1, x2;

    //private BoxCollider2D boxcol;

    //// Start is called before the first frame update
    //void Awake()
    //{
    //    boxcol = GetComponent<BoxCollider2D>();

    //    x1 = transform.position.x - boxcol.bounds.size.x / 2f;
    //    x2 = transform.position.x + boxcol.bounds.size.x / 2f;

    //    //create enem

    //    float startHeight = (float)Screen.height / 2.0f;
    //    float startWidth = (float)Screen.width / 2.0f;

    //    EnemyClass mook = new EnemyClass();
    //    mook.initializeSequenceMook();
    //    mook.setPosition(new Vector2(startWidth, startHeight));
    //    mook.setName("mook" + 0);

    //    EnemyClass boss = new EnemyClass();
    //    boss.initializeSequenceBoss();
    //    boss.setPosition(new Vector2(startWidth, startHeight));
    //    boss.setName("boss");

    //    EnemyClass temp = new EnemyClass();

    //    for (int i = 0; i < 10; i++)
    //    {
    //        if (i == 0)
    //        {
    //            enem = mook;
    //            temp = enem;
    //        }
    //        else if (i < 9 && i > 0) //link mooks together
    //        {
    //            mook = new EnemyClass();
    //            mook.initializeSequenceMook();
    //            mook.setPosition(new Vector2(startWidth, startHeight));
    //            mook.setName("mook" + i);

    //            temp.setNextEnemy(mook);
    //            mook.setPrevEnemy(temp);
    //            temp = mook;
    //        }
    //        else if (i == 9) //link boss
    //        {
    //            temp.setNextEnemy(boss);
    //            boss.setPrevEnemy(enem);
    //            enem.setPrevEnemy(boss);
    //        }

    //    }
    //    checkEnemList();
    //}

    //void Start()
    //{
    //    for (int i = 0; i < 10; i++)
    //    {
    //        if (i < 9)
    //            StartCoroutine(SpawnMook(1f, i));
    //        else if (i == 9)
    //            StartCoroutine(SpawnBoss(1f, i));
    //    }
    //}

    //// Update is called once per frame
    //void Update()
    //{

    //}

    //EnemyClass SearchEnemy(int index)
    //{
    //    int i = 0;
    //    EnemyClass res = enem;
    //    while (i < index)
    //    {
    //        res = res.getNextEnemy();
    //        i++;
    //    }
    //    return res;
    //}



    //IEnumerator SpawnMook(float time, int index)
    //{
    //    yield return new WaitForSecondsRealtime(time);

    //    float x1 = transform.position.x - boxcol.bounds.size.x / 2f;
    //    float x2 = transform.position.x + boxcol.bounds.size.x / 2f;

    //    Vector3 temp = transform.position;
    //    temp.x = Random.Range(x1, x2);
    //    temp.z = 0;

    //    Instantiate(mook, temp, Quaternion.identity);


    //        StartCoroutine(SpawnMook(Random.Range(1f, 2f), index));

    //}

    //IEnumerator SpawnBoss(float time, int index)
    //{
    //    yield return new WaitForSecondsRealtime(time);

    //    float x1 = transform.position.x - boxcol.bounds.size.x / 2f;
    //    float x2 = transform.position.x + boxcol.bounds.size.x / 2f;

    //    Vector3 temp = transform.position;
    //    temp.x = Random.Range(x1, x2);
    //    temp.z = 0;

    //    Instantiate(boss, temp, Quaternion.identity);


    //        StartCoroutine(SpawnBoss(Random.Range(1f, 2f), index));

    //}

    //void checkEnemList()
    //{
    //    EnemyClass temp = enem;
    //    for (int i = 0; i < 10; i++)
    //    {
    //        Debug.Log(temp.getName() + i);
    //        temp = enem.getNextEnemy();
    //    }
    //}
}
