using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour
{
    public class EnemyClass
    {
        private EnemyClass nextEnemy;
        private EnemyClass prevEnemy;
        private int hp;
        private int[] seq;
        private string enname;
        private string type;
        private Vector2 speed;
        private Vector2 direction;
        private Vector2 position;
        private Sprite spr;
        private static System.Random r = new System.Random();

        public EnemyClass()
        {
            hp = 0;
        }

        /*public static EnemyClass addEnemy(GameObject go)
        {
            EnemyClass obj = go.AddComponent("EnemyClass");
            obj.setHp(0);
            return obj;
        }*/

        public int getHp()
        {
            return hp;
        }

        public void setHp(int hp)
        {
            this.hp = hp;
        }

        public int getSeq(int index)
        {
            return seq[index];
        }

        public string getName()
        {
            return enname;
        }

        public void setName(string name)
        {
            this.enname = name;
        }

        public string getType()
        {
            return type;
        }

        public void initializeEnemySprite(Sprite[] sprlist)
        {
            switch (type)
            {
                case "mook": break;
                case "boss1": break;
                case "boss2": break;
            }
        }

        public void setPosition(Vector2 position)
        {
            this.position = position;
        }

        public Vector2 getPosition()
        {
            return position;
        }

        public void setSpeed(Vector2 speed)
        {
            this.speed = speed;
        }

        public void setDirection(Vector2 direction)
        {
            this.direction = direction;
        }

        public void setNextEnemy(EnemyClass next)
        {
            this.nextEnemy = next;
        }

        public void setPrevEnemy(EnemyClass prev)
        {
            this.prevEnemy = prev;
        }

        public EnemyClass getNextEnemy()
        {
            return this.nextEnemy;
        }

        public EnemyClass getPreviousEnemy()
        {
            return this.prevEnemy;
        }

        public void initializeSequenceBoss()
        {
            this.seq = new int[4];
            for (int i = 0; i < 4; i++)
            {
                this.seq[i] = r.Next(0, 5);
            }
        }

        public void initializeSequenceMook()
        {
            this.seq = new int[2];
            this.seq[0] = r.Next(0, 5);
            this.seq[1] = 0;
            /*for (int i = 0; i < 4; i++)
            {
                this.seq[i] = r.Next(1, 5);
            }*/
        }

        /*// Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }*/
    }

    public GameObject hA;
    public GameObject engo;
    public int hitArrow;
    public int hp;
    private EnemyClass enem;
    public Sprite[] enempics;

    // Start is called before the first frame update
    void Start()
    {
        //init screen coords
        float startHeight = (float)Screen.height / 2.0f;
        float startWidth = (float)Screen.width / 2.0f;

        //create 9 mooks + 1 boss
        EnemyClass mook = new EnemyClass();
        mook.initializeSequenceMook();
        mook.setPosition(new Vector2(startWidth, startHeight));
        mook.setSpeed(new Vector2(0, 0));
        mook.setName("mook"+ 0);

        EnemyClass boss = new EnemyClass();
        boss.initializeSequenceBoss();
        boss.setPosition(new Vector2(startWidth, startHeight));
        boss.setSpeed(new Vector2(0, 0));
        boss.setName("boss");

        EnemyClass temp = new EnemyClass();

        for (int i = 0; i < 10; i++)
        {
            if (i == 0)
            {
                enem = mook;
                temp = enem;
            }
            else if (i < 9 && i > 0) //link mooks together
            {
                mook = new EnemyClass();
                mook.initializeSequenceMook();
                mook.setPosition(new Vector2(startWidth, startHeight));
                mook.setSpeed(new Vector2(0, 0));
                mook.setName("mook" + i);

                temp.setNextEnemy(mook);
                mook.setPrevEnemy(temp);
                temp = mook;
            }
            else if (i == 9) //link boss
            {
                temp.setNextEnemy(boss);
                boss.setPrevEnemy(enem);
                enem.setPrevEnemy(boss);
            }

        }
        checkEnemList();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void checkEnemList()
    {
        EnemyClass temp = enem;
        for (int i = 0; i < 10; i++)
        {
            Debug.Log(temp.getName() + i);
            temp = enem.getNextEnemy();
        }
    }
}
