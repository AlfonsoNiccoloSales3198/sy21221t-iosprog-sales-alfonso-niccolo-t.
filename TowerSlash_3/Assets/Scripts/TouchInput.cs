using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Direction
{
    left, right, up, down
}
public class TouchInput : MonoBehaviour
{
    private Vector2 Touchpos;
    private Vector2 Touchcur;
    private Vector2 Touchend;
    private bool TS = false;

    private float width;
    private float height;

    public float swipeRange;
    public float tapRange;
    public string textValue;
    public Text textElement;
    public GameObject enemies;

    public Direction myDirection;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Awake()
    {
        width = (float)Screen.width / 2.0f;
        height = (float)Screen.height / 2.0f;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.touchCount == 1)
        {
            Touch myTouch = Input.GetTouch(0);

            if (myTouch.phase == TouchPhase.Began) 
            {
                Touchcur = myTouch.rawPosition;
                textElement.text = "input start!";
            }
            if (myTouch.phase == TouchPhase.Moved)
            {
                //Touchpos = Touchcur - myTouch.position;
                Touchpos = myTouch.deltaPosition;
                Touchend = Touchcur + Touchpos;

                if (Touchcur.y < Touchend.y && Touchcur.x == Touchend.x)
                {
                    myDirection = Direction.up;
                    textValue = "Up";
                   
                }
                if (Touchcur.y > Touchend.y && Touchcur.x == Touchend.x)
                {
                    myDirection = Direction.down;
                    textValue = "Down";
                   
                }
                if (Touchcur.x < Touchend.x && Touchcur.y == Touchend.y)
                {
                    myDirection = Direction.right;
                    textValue = "Right";
                   
                }
                if (Touchcur.x > Touchend.x && Touchcur.y == Touchend.y)
                {
                    myDirection = Direction.left;
                    textValue = "Left";
                    
                }
                textElement.text = textValue;
            }
            if (myTouch.phase == TouchPhase.Ended)
            {
                textElement.text = "No input";
            }
        }
        
    }
}
