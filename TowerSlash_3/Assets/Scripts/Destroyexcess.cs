using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyexcess : MonoBehaviour
{
    public Player player;
    public int j = 0;
    private void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == "mookDown")
        {
            target.gameObject.SetActive(false);
            Destroy(target.gameObject);
            player.healthP -= 1;
        }
        else if (target.tag == "mookLeft")
        {
            target.gameObject.SetActive(false);
            Destroy(target.gameObject);
            player.healthP -= 1;
        }
        else if (target.tag == "mookRight")
        {
            target.gameObject.SetActive(false);
            Destroy(target.gameObject);
            player.healthP -= 1;
        }
        else if (target.tag == "mookUp")
        {
            target.gameObject.SetActive(false);
            Destroy(target.gameObject);
            player.healthP -= 1;
        }
    }
}
