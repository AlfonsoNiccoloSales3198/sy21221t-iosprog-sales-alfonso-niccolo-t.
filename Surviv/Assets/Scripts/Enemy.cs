using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float EnemyHP = 100f;
    public GameObject enem;
    public EnemySpawner mySpawner;
    public AIMovement ail;
    // Start is called before the first frame update
    void Start()
    {
        enem = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (EnemyHP <= 0)
        {
            //mySpawner.j--;
            //Debug.Log(mySpawner.j);
            //gameObject.SetActive(false);
            
            Destroy(enem);
            
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject other = collision.gameObject;
        if (other.CompareTag("bullet"))
        {
            EnemyHP -= 10;
        }
    }
}
