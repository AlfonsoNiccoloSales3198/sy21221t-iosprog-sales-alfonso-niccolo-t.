using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour
{
    public void Starters()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Escape()
    {
        Application.Quit();
        
    }

    public void returner()
    {
        SceneManager.LoadScene(0);
    }

}
