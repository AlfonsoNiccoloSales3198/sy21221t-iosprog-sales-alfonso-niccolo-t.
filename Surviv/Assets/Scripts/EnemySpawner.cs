using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private float spawnRadius = 7, time = 1.5f;
    int i = 0;
   

    public GameObject[] enemies;
    public Enemy enemy;

        
    void Start()
    {
        

        while (i < 10)
        {
            StartCoroutine(SpawnAnEnemy());
            i++;
            
        }
    }

    private void Update()
    {
        
        if (GameObject.FindGameObjectsWithTag("enemies").Length <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    IEnumerator SpawnAnEnemy()
    {
        Vector2 spawnPos = GameObject.Find("Player").transform.position + new Vector3(Random.Range(1, 30),Random.Range(1, 30), 0);
        spawnPos += Random.insideUnitCircle.normalized * spawnRadius;

        Instantiate(enemies[Random.Range(0, enemies.Length)], spawnPos, Quaternion.identity);
        yield return new WaitForSeconds(time);
        
        
    }

}
