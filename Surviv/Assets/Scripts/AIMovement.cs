using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIMovement : MonoBehaviour
{
    [SerializeField] Transform target;
    NavMeshAgent agent;
    public float rotSpeed = 500f;
    public PlayerMovement playerMovement;
    public float ammo = 10;
    public Transform firePoint;
    public GameObject bulletPrefab;
    public GameObject fire2;
    public Enemy enem2;
    bool shot = false;

    public float bulletForce = 20f;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("Player").transform;
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updateUpAxis = false;
    }
    void Seek(Vector3 location)
    {
        agent.SetDestination(location);
    }
    void Pursue()
    {
        //Vector3 targetDirection = target.transform.position - this.transform.position;
        //Vector3 distPos = this.transform.position - target.transform.position;

        //float lookAhead = targetDirection.magnitude / (agent.speed + playerMovement.moveSpeed);

        //Seek(target.transform.position + target.transform.forward * lookAhead);

        var direction = target.position - agent.transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        agent.transform.rotation = Quaternion.Slerp(agent.transform.rotation, q, rotSpeed * Time.deltaTime);
        agent.SetDestination(target.position);

    }

    // Update is called once per frame
    void Update()
    {
       
        if (Vector3.Distance(target.transform.position, this.transform.position) < 5.0f)
        {
            shot = false;
            StartCoroutine(WaitAndExecute());
        }
        else if (Vector3.Distance(target.transform.position, this.transform.position) < 10.0f)
        {
            Pursue();
        }
    }


    IEnumerator WaitAndExecute()
    {
        

        if (this.gameObject.tag == "enemies")
        {
            yield return new WaitForSeconds(2f);
            Fire();
            shot = true;
            if (shot == true)
            {
                StopCoroutine(WaitAndExecute());
            }
        }
        else if (this.gameObject.tag == "shotshooter")
        {
            yield return new WaitForSeconds(2f);
            Fire();
            fireD();
            shot = true;
            if (shot == true)
            {
                StopCoroutine(WaitAndExecute());
            }
        }

    }
    void Fire()    
    {
        if (ammo >= 1)
        {
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
            ammo--;
        }
        else if (ammo <= 0)
        {
            Debug.Log("Out of Ammo");
            Reload();
        }
    }

    void fireD()
    {
        if (ammo >= 1)
        {
            GameObject bullet = Instantiate(bulletPrefab, fire2.transform.position, fire2.transform.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
            ammo--;
        }
        else if (ammo <= 0)
        {
            Debug.Log("Out of Ammo");
            Reload();
        }
    }

    public void Reload()
    {
        ammo += 10;
    }

}
