using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 5f;

    public Rigidbody2D rb;
    public Camera cam;

    public Joystick joystick;
    public Joystick joy2;

    Vector2 movement;
    Vector3 mousePos;
    // Update is called once per frame
    void Update()
    {
        movement.x = joystick.Horizontal;
        movement.y = joystick.Vertical;

        mousePos = new Vector3(joy2.Horizontal, joy2.Vertical);
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);

        if (joy2.Horizontal != 0 || joy2.Vertical !=0)
        { 
            transform.rotation = Quaternion.LookRotation(Vector3.forward, mousePos);
        }
        
    }
}
