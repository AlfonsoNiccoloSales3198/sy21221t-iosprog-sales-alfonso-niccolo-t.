using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shooting : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    public Transform firePoint2;
    public Transform firePoint3;

    public float bulletForce = 20f;
    public float ammo = 0;
    public float ammo2 = 0;

    public Text ammoText;
    public Text ammoTwo;
    public Text weapon;
    public Text weapon2;
    bool carry = false;
    bool carry2 = false;

    public float gunType = 0;
    public float gunTwo = 0;
    public float timeBtwShots = 0.5f;
    public float spreadAngle = 2.0f;

    float nextShot = 0.0f;


    // Update is called once per frame
    void Update()
    {
        ammoText.text = ammo.ToString();
        ammoTwo.text = ammo2.ToString();
        if (gunType == 1)
        {
            weapon.text = "Handgun";
            carry = true;
        }
        if (gunTwo == 2)
        {
            weapon2.text = "Auto Rifle";
            carry2 = true;
        }
        if (gunTwo == 3)
        {
            weapon2.text = "Shotgun";
            carry2 = true;
        }
    }

    public void Shoot()
    {
        if (ammo >= 1 && gunType == 1)
        {
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
            
            ammo--;
        }
        else if (ammo <= 0)
        {
            ammo = 0;
            Debug.Log("Out of Ammo");
        }
    }

    
    public void Shoot2()
    {
        if (ammo2 >= 1 && gunTwo == 2)
        {
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
            ammo2--;
            bullet = Instantiate(bulletPrefab, firePoint2.position, firePoint2.rotation);
            rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(firePoint2.up * bulletForce, ForceMode2D.Impulse);
            ammo2--;
        
            
        }
        else if (ammo2 <= 0)
        {
            ammo2 = 0;
            Debug.Log("Out of Ammo");
        }

        if (ammo2 >= 1 && gunTwo == 3 && Time.time > nextShot)
        {
            
                
                GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
                Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
                rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
            ammo2--;
            bullet = Instantiate(bulletPrefab, firePoint3.position, firePoint3.rotation);
            rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
            ammo2--;
              
            
        }
        else if (ammo2 <= 0)
        {
            ammo2 = 0;
            Debug.Log("Out of Ammo");
        }
    }

    public void shooterShots()
    {
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
    }

    public void Reload()
    {
        if (carry == true)
        {
            ammo += 10;
            if (ammo >= 20)
            {
                ammo = 20;
            }
        }
        
        if (carry2 == true)
        {
            ammo2 += 10;
            if (ammo2 >= 20)
            {
                ammo2 = 20;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject other = collision.gameObject;
        if (other.CompareTag("pistol"))
        {
            gunType = 1;
            ammo = 10;
            Destroy(other);
        }
        else if (other.CompareTag("auto"))
        {
            gunTwo = 2;
            ammo2 = 10;
            Destroy(other);
        }
        else if (other.CompareTag("shot"))
        {
            gunTwo = 3;
            ammo2 = 10;
            Destroy(other);
        }
    }
}
